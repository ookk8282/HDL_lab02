/*
1-digit BCD number Up/Down Counter
do nothing if not $en
dir: increment if dir == 1, otherwise decrement
cout: carry out when increment, while
      borrow    when decrement
*/
module lab2_1(inputs, en, dir, outputs, cout);
    input [3:0] inputs;
    input en, dir;
    output reg [3:0] outputs;
    output cout;

    assign cout = (~en) ? 1'b0 :
                  dir ? inputs == 4'b1001 : inputs == 4'b0000;
    always@(inputs or en or dir) begin
        outputs <= inputs;
        if (en) begin
            outputs <= dir ? inputs + 1 : inputs - 1;
            if (cout)
                outputs <= dir ? 4'b0000 : 4'b1001;
            else if (inputs > 4'b1001)
                outputs <= 4'b0000;
        end
    end
endmodule
