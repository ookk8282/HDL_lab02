`timescale 1ns / 1ps

module lab2_1_t;
    reg [3:0] inputs;
    reg en, dir;
    wire [3:0] outputs;
    wire cout;
    reg pass;

    lab2_1 bcd_counter(inputs, en, dir, outputs, cout);

    initial begin
        #0 pass = 1'b1; inputs = 4'b0000; en = 1'b0; dir = 1'b0;
        $display("Starting simulation");
        //$monitor("%g\ten=%b\tdir=%b\tinputs=%d\toutputs=%d\tcout=%b", $time, en, dir, inputs, outputs, cout);
        #128 $display("%g Terminating simulation", $time);
        if (pass) $display("=== [PASS!] ===");
        else      $display("XXX  ERROR  XXX");
        $finish;
    end

    always #2  inputs = inputs + 1;
    always #32 dir    = dir    + 1;
    always #64 en     = en     + 1;

    always @(*) begin
        #1
        if (en == 1'b0)                     assert_results(inputs,   1'b0);
        else if (inputs > 4'b1001)          assert_results(4'b0000,  1'b0);
        else if (dir && inputs != 4'b1001)  assert_results(inputs+1, 1'b0);
        else if (!dir && inputs != 4'b0000) assert_results(inputs-1, 1'b0);
        else if (dir)                       assert_results(4'b0000,  1'b1);
        else                                assert_results(4'b1001,  1'b1);
    end

    task assert_results;
        input [3:0] expected_outputs;
        input expected_cout;
        reg local_pass;

        begin
            local_pass = 1'b1;
            if (outputs != expected_outputs) begin
                local_pass = 1'b0;
                $display("outputs = %d, expected %d",
                          outputs, expected_outputs);
            end
            if (cout    != expected_cout) begin
                local_pass = 1'b0;
                $display("cout = %b, expected",
                          cout, expected_cout);
            end
            if (!local_pass) begin
                pass = 1'b0;
                $display("\ten = %b, dir = %b, inputs = %d",
                            en,      dir,      inputs);
            end
        end
    endtask
endmodule
