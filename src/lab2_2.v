/*
* 1-digit BCD Up/Down Counter
* negative-edge triggered
* async reset
*/

module lab2_2(clk, reset, en, dir, BCD, cout);
    input clk, reset, en, dir;
    output reg [3:0] BCD;
    output cout;
    wire [3:0] next_BCD;

    lab2_1 counter(
        .inputs(BCD),
        .en(en),
        .dir(dir),
        .outputs(next_BCD),
        .cout(cout));

    always@(negedge clk or posedge reset) begin
        BCD <= reset ? 4'b0000 : next_BCD;
    end
endmodule
